package com.example.smsforwarder.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [SmsInfo::class], version = 2, exportSchema = false)
@TypeConverters(Converter::class)
abstract class SmsDatabase : RoomDatabase() {

    abstract val smsDatabaseDao: SmsDatabaseDao

    companion object {

        fun getInstance(context: Context): SmsDatabase {
            return Room.databaseBuilder(
                            context.applicationContext,
                            SmsDatabase::class.java,
                            "sms_database"
                        ).fallbackToDestructiveMigration()
                .build()
        }
    }
}

