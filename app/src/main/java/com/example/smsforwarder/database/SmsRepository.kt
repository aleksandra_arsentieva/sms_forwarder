package com.example.smsforwarder.database

import androidx.lifecycle.LiveData
import kotlinx.coroutines.flow.Flow

interface SmsRepository {
    suspend fun saveSms(sms: String)

    fun getReadySms(): Flow<List<SmsInfo>>

    fun getAllSms(): LiveData<List<SmsInfo>>

    suspend fun updateSms(sms: SmsInfo)

    suspend fun setStatusReady(smsId: Long)

    suspend fun clearSms()
}

class RoomSmsRepository(private val database: SmsDatabaseDao): SmsRepository {

    override suspend fun saveSms(sms: String) {
        val newSmsInfo = SmsInfo(smsCode = sms, sendStatus = Status.READY)
        database.insert(newSmsInfo)
    }

    override fun getReadySms(): Flow<List<SmsInfo>> {
        return database.getReadySms()
    }

    override fun getAllSms(): LiveData<List<SmsInfo>> {
        return database.getAllSms()
    }

    override suspend fun updateSms(sms: SmsInfo) {
        database.update(sms)
    }

    override suspend fun setStatusReady(smsId: Long) {
        database.setStatusReady(smsId)
    }

    override suspend fun clearSms() {
        database.clear()
    }


}
