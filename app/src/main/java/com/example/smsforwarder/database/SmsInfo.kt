package com.example.smsforwarder.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter

@Entity(tableName = "sms_table")
data class SmsInfo(
    @PrimaryKey(autoGenerate = true)
    var smsId: Long = 0L,

    @ColumnInfo(name = "receiving_time_milli")
    val receivingTimeMilliseconds: Long = System.currentTimeMillis(),

    @ColumnInfo(name = "sms_code")
    var smsCode: String = "",

    @ColumnInfo(name = "send_status")
    var sendStatus: Status = Status.READY
)

enum class Status {
    READY,
    SEND,
    FAILED
}

class Converter {

    @TypeConverter
    fun fromStatus(priority: Status): String {
        return priority.name
    }

    @TypeConverter
    fun toStatus(priority: String): Status {
        return Status.valueOf(priority)
    }

}