package com.example.smsforwarder.database

import androidx.lifecycle.LiveData
import androidx.room.*
import kotlinx.coroutines.flow.Flow


@Dao
interface SmsDatabaseDao {

    @Insert
    suspend fun insert(smsInfo: SmsInfo)

    @Update
    suspend fun update(smsInfo: SmsInfo)


    @Query("SELECT * from sms_table WHERE smsId = :key")
    suspend fun get(key: Long): SmsInfo?

    @Query("DELETE FROM sms_table")
    suspend fun clear()

    @Query("SELECT * FROM sms_table ORDER BY smsId DESC")
    fun getAllSms(): LiveData<List<SmsInfo>>

    @Query("SELECT * from sms_table WHERE send_status = 'READY'")
    fun getReadySms(): Flow<List<SmsInfo>>

    @Query("UPDATE sms_table SET send_status = 'READY' WHERE smsId = :key")
    suspend fun setStatusReady(key: Long)
}
