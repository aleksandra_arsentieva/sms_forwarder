package com.example.smsforwarder.viewmodel

import android.app.Application
import androidx.lifecycle.*
import com.example.smsforwarder.SmsApplication
import com.example.smsforwarder.settings.LiveSharedPreferences

import com.example.smsforwarder.utils.SmsHandler

class SmsViewModel(
    application: Application,
    private val smsForwardingSettings: LiveSharedPreferences
): AndroidViewModel(application) {

    private val smsHandler: SmsHandler
        get() = this.getApplication<SmsApplication>().getSmsHandler()

    val allSms = smsHandler.getAllSms()

    fun getSmsForwardingSettings() = smsForwardingSettings

}