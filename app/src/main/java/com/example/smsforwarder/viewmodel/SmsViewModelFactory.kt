package com.example.smsforwarder.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.smsforwarder.settings.LiveSharedPreferences

class SmsViewModelFactory(
    private val application: Application,
    private val smsForwardingSettings: LiveSharedPreferences
): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SmsViewModel::class.java)) {
            return SmsViewModel(application, smsForwardingSettings) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}