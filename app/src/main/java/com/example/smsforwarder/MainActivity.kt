package com.example.smsforwarder

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.*
import com.example.smsforwarder.databinding.ActivityMainBinding
import com.example.smsforwarder.recyclerview.SmsItemAdapter
import com.example.smsforwarder.service.SmsSendService
import com.example.smsforwarder.settings.LiveSharedPreferences
import com.example.smsforwarder.settings.SettingsActivity
import com.example.smsforwarder.utils.SmsHandler
import com.example.smsforwarder.utils.StartServiceWorker
import com.example.smsforwarder.utils.StopServiceWorker
import com.example.smsforwarder.viewmodel.SmsViewModel
import com.example.smsforwarder.viewmodel.SmsViewModelFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    private val MY_REQUEST_RECEIVE_SMS_RESULT = 1
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: SmsViewModel
    private lateinit var smsHandler: SmsHandler
    private lateinit var smsForwardingSettings: LiveSharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        smsForwardingSettings = LiveSharedPreferences(getSharedPreferences("settings", Context.MODE_PRIVATE))
        viewModel = ViewModelProvider(this,
            SmsViewModelFactory(
                this.application,
                smsForwardingSettings)
        ).get(SmsViewModel::class.java)
        smsHandler = (applicationContext as SmsApplication).getSmsHandler()
        binding.smsViewModel = viewModel
        binding.lifecycleOwner = this

        binding.smsList.layoutManager = LinearLayoutManager(this)
        val adapter = SmsItemAdapter()
        binding.smsList.adapter = adapter
        viewModel.allSms.observe(this, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })

        binding.errorView.setOnClickListener {
            requestSmsReceivePermission()
        }

        binding.errorGoToSettings.setOnClickListener {
            startActivityForResult(Intent(android.provider.Settings.ACTION_SETTINGS), 0)
        }

        viewModel.getSmsForwardingSettings().observe(this, Observer {
            if (it == true)
                SmsSendService.startService(this, "Foreground Service is running...")
            else
                SmsSendService.stopService(this)
        })

        binding.switchForwarding.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                smsHandler.setSmsForwarding(true)
            } else {
                smsHandler.setSmsForwarding(false)
            }
        }

        binding.clearList.setOnClickListener{
            GlobalScope.launch {
                smsHandler.clearAllSms()
            }
        }
        startWorkManager(9, StartServiceWorker::class.java, "Start_service_worker")
        startWorkManager(21, StopServiceWorker::class.java, "Stop_service_worker")
        setSupportActionBar(binding.toolbar)
        checkForSmsPermission()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.settings_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_settings) {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }
        return true
    }

    private fun checkForSmsPermission() {
        val settings = smsHandler.getSmsPermission()
        if (settings != "") {
            if (settings == "NOT_ALLOWED") {
                showErrorView()
            }
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {
                requestSmsReceivePermission()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray) {

        when (requestCode) {
            MY_REQUEST_RECEIVE_SMS_RESULT -> {
                if (permissions[0].equals(Manifest.permission.RECEIVE_SMS, ignoreCase = true)
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    smsHandler.setSmsPermission("ALLOWED")
                    binding.errorView.visibility = GONE

                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.RECEIVE_SMS)) {
                        // now, user has denied permission (but not permanently!)
                        smsHandler.setSmsPermission("NOT_ALLOWED")
                        showErrorView()
                    }
                    else {
                        // now, user has denied permission permanently!
                        smsHandler.setSmsPermission("NOT_ALLOWED")
                        showGoToSettingsError()
                    }
                }
            }
        }
    }

    private fun showErrorView() {
        binding.errorView.setText(R.string.permission_not_granted)
        binding.errorView.visibility = VISIBLE
    }

    private fun showGoToSettingsError() {
        binding.errorView.visibility = GONE
        binding.errorGoToSettings.setText(R.string.should_go_to_settings)
        binding.errorGoToSettings.visibility = VISIBLE
    }

    private fun requestSmsReceivePermission() {
        ActivityCompat.requestPermissions(
            this, arrayOf(Manifest.permission.RECEIVE_SMS),
            MY_REQUEST_RECEIVE_SMS_RESULT)
    }

    fun startWorkManager(dayTime: Int, worker: Class<out Worker>, tag: String) {
        val repeatInterval = 1 // In days

        val flexTime: Long = calculateFlex(dayTime, repeatInterval)

        val myConstraints: Constraints = Constraints.Builder()
            .build()

        val workRequest = PeriodicWorkRequest.Builder(
            worker,
            repeatInterval.toLong(), TimeUnit.DAYS,
            flexTime, TimeUnit.MILLISECONDS
        ).setConstraints(myConstraints)
            .build()

        WorkManager.getInstance().enqueueUniquePeriodicWork(
            tag,
            ExistingPeriodicWorkPolicy.REPLACE,
            workRequest
        )
    }
    private fun calculateFlex(hourOfTheDay: Int, periodInDays: Int): Long {

        val cal1: Calendar = Calendar.getInstance()
        cal1.set(Calendar.HOUR_OF_DAY, hourOfTheDay)
        cal1.set(Calendar.MINUTE, 0)
        cal1.set(Calendar.SECOND, 0)

        val cal2: Calendar = Calendar.getInstance()
        if (cal2.getTimeInMillis() < cal1.getTimeInMillis()) {
            cal2.setTimeInMillis(
                cal2.getTimeInMillis() + TimeUnit.DAYS.toMillis(
                    periodInDays.toLong()
                )
            )
        }
        val delta: Long = cal2.getTimeInMillis() - cal1.getTimeInMillis()
        return if (delta > PeriodicWorkRequest.MIN_PERIODIC_FLEX_MILLIS) delta else PeriodicWorkRequest.MIN_PERIODIC_FLEX_MILLIS
    }

}
