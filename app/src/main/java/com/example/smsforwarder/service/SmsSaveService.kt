package com.example.smsforwarder.service

import android.content.Context
import android.content.Intent
import androidx.core.app.JobIntentService
import com.example.smsforwarder.SmsApplication

val JOB_ID : Int = 1000
fun enqueueWork(context: Context, work: Intent) {
    JobIntentService.enqueueWork(context, SmsSaveService::class.java,
        JOB_ID, work)
}

class SmsSaveService: JobIntentService() {

    override fun onHandleWork(intent: Intent) {
        val smsHandler = (applicationContext as SmsApplication).getSmsHandler()
        val messageBody = intent?.extras?.getString("sms_body")
        val messageAddress = intent?.extras?.getString("sms_address")
        smsHandler.handleSms(messageBody, messageAddress)
    }
}