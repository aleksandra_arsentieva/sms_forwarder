package com.example.smsforwarder.service

import android.app.ActivityManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.example.smsforwarder.MainActivity
import com.example.smsforwarder.R
import com.example.smsforwarder.SmsApplication
import com.example.smsforwarder.utils.SmsSender
import com.example.smsforwarder.utils.createNotificationChannel
import kotlinx.coroutines.*


class SmsSendService: Service() {
    private val CHANNEL_ID = "ForegroundService SmsSending"
    private val scope = CoroutineScope(Dispatchers.IO + Job())

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        //do heavy work on a background thread
        val input = intent?.getStringExtra("inputExtra")
        createNotificationChannel(CHANNEL_ID, this,"ForegroundService SmsSending")
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Sms forward service started")
            .setContentText(input)
            .setSmallIcon(R.drawable.chat)
            .setContentIntent(pendingIntent)
            .setGroup("Foreground service")
            .build()
        startForeground(1, notification)
        scope.launch {
            startSmsSender()
        }
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onDestroy() {
        scope.cancel()
        super.onDestroy()
    }
    private suspend fun startSmsSender() {
        val sender = SmsSender(
            (applicationContext as SmsApplication).getSmsHandler(),
            applicationContext
        )
        sender.startSend()
    }

    companion object {
        fun startService(context: Context, message: String) {
            val startIntent = Intent(context, SmsSendService::class.java)
            startIntent.putExtra("inputExtra", message)
            if (!context.isServiceRunning(SmsSendService::class.java)) {
                ContextCompat.startForegroundService(context, startIntent)
            }
        }
        fun stopService(context: Context) {
            val stopIntent = Intent(context, SmsSendService::class.java)
            context.stopService(stopIntent)
        }

        fun <T> Context.isServiceRunning(service: Class<T>) =
            (getSystemService(ACTIVITY_SERVICE) as ActivityManager)
                .getRunningServices(Integer.MAX_VALUE)
                .any { it.service.className == service.name }
    }
}