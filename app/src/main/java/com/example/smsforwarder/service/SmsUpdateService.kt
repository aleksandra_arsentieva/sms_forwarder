package com.example.smsforwarder.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.example.smsforwarder.SmsApplication
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SmsUpdateService: Service() {

    override fun onBind(p0: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val smsHandler = (applicationContext as SmsApplication).getSmsHandler()
        val messageId = intent.extras?.getLong("sms_id")
        GlobalScope.launch {
            if (messageId != null) {
                smsHandler.updateStatusSmsWithId(messageId)
            }
        }
        return START_STICKY
    }
}