package com.example.smsforwarder

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.SmsMessage
import androidx.annotation.RequiresApi
import com.example.smsforwarder.service.SmsSaveService
import com.example.smsforwarder.service.enqueueWork


class SmsBroadcastReceiver : BroadcastReceiver() {

    val pdu_type = "pdus"

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onReceive(context: Context, intent: Intent) {
        val bundle = intent.extras
        val msgs: Array<SmsMessage?>
        val format = bundle!!.getString("format")
        val pdus = bundle.get(pdu_type) as Array<Any>
        msgs = arrayOfNulls(pdus.size)
        for (i in msgs.indices)
            {
                msgs[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray, format)
                val messageBody = msgs[i]?.messageBody
                val messageAddress = msgs[i]?.displayOriginatingAddress
                val mIntent = Intent(context, SmsSaveService::class.java)
                mIntent.putExtra("sms_body", messageBody)
                mIntent.putExtra("sms_address", messageAddress)
                enqueueWork(context, mIntent)
                abortBroadcast()
            }
        }
}
