package com.example.smsforwarder.recyclerview

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.smsforwarder.R
import com.example.smsforwarder.database.SmsInfo
import com.example.smsforwarder.database.Status
import java.text.SimpleDateFormat
import java.util.*

@BindingAdapter("imageSmsStatus")
fun ImageView.setSleepImage(item: SmsInfo) {
    setImageResource(when (item.sendStatus) {
        Status.FAILED -> R.drawable.not_send
        Status.SEND -> R.drawable.ok_send
        Status.READY -> R.drawable.readytosend
    })
}

@BindingAdapter("smsCode")
fun TextView.setSmsCode(item: SmsInfo?) {
    item?.let {
        text = item.smsCode
    }
}

@BindingAdapter("smsDatetime")
fun TextView.setSmsDatetime(item: SmsInfo?) {
    item?.let {
        text = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault()).format(item.receivingTimeMilliseconds)
    }
}