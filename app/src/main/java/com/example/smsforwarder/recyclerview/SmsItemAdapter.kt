package com.example.smsforwarder.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.smsforwarder.database.SmsInfo
import com.example.smsforwarder.databinding.ListItemSmsInfoBinding


class SmsItemAdapter(): ListAdapter<SmsInfo, SmsItemAdapter.ViewHolder>(SmsDiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: ListItemSmsInfoBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SmsInfo) {
            binding.sms = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemSmsInfoBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class SmsDiffCallback : DiffUtil.ItemCallback<SmsInfo>() {

    override fun areContentsTheSame(oldItem: SmsInfo, newItem: SmsInfo): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: SmsInfo, newItem: SmsInfo): Boolean {
        return oldItem.smsId == newItem.smsId
    }
}
