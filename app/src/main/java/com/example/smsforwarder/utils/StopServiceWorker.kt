package com.example.smsforwarder.utils

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.smsforwarder.SmsApplication
import com.example.smsforwarder.service.SmsSendService

class StopServiceWorker(val context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {

    override fun doWork(): Result {
        return try {
            try {
                SmsSendService.stopService(context)
                (applicationContext as SmsApplication).getSmsHandler().setSmsForwarding(true)
                Result.success()
            } catch (e: Exception) {
                Log.d("MyWorker", "exception in doWork ${e.message}")
                Result.failure()
            }
        } catch (e: Exception) {
            Result.failure()
        }
    }
}