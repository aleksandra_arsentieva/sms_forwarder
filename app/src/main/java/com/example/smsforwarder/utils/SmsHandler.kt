package com.example.smsforwarder.utils

import com.example.smsforwarder.BuildConfig
import com.example.smsforwarder.database.SmsInfo
import com.example.smsforwarder.database.SmsRepository
import com.example.smsforwarder.settings.SettingsProvider
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SmsHandler(
    private val settingsProvider: SettingsProvider,
    private val smsRepository: SmsRepository
) {

    private fun saveSmsToDb(sms: String) {
        GlobalScope.launch {
            smsRepository.saveSms(sms)
        }
    }

    fun getReadySms() = smsRepository.getReadySms()

    suspend fun updateSms(sms: SmsInfo) = smsRepository.updateSms(sms)

    fun getAllSms() = smsRepository.getAllSms()

    suspend fun updateStatusSmsWithId(smsId: Long) = smsRepository.setStatusReady(smsId)

    suspend fun clearAllSms() = smsRepository.clearSms()

    fun getPhoneNumber(): String? {
        return if (settingsProvider.phoneNumber == "")
            BuildConfig.PHONE_NUMBER_DEFAULT
        else
            settingsProvider.phoneNumber
    }

    fun getTelegramUrl(): String? {
        return if (settingsProvider.telegramUrl == "")
            BuildConfig.TELEGRAM_TO_BOT_URL
        else
            settingsProvider.telegramUrl
    }

    fun getSendOperation(): String? {
        return if (settingsProvider.sendOperation == "")
            BuildConfig.SEND_OPERATION
        else
            settingsProvider.sendOperation
    }

    fun getApiToken(): String? {
        return if (settingsProvider.apiToken == "")
            BuildConfig.API_TOKEN
        else
            settingsProvider.apiToken
    }

    fun getChatId(): String? {
        return if (settingsProvider.chatId == "")
            BuildConfig.CHAT_ID
        else
            settingsProvider.chatId
    }

    fun getRegexp(): String? {
        return if (settingsProvider.regexp == "")
            BuildConfig.REGEXP_DEFAULT
        else
            settingsProvider.regexp
    }

    fun getSmsPermission(): String? {
        return settingsProvider.sms_permessions
    }

    fun getSmsForwarding(): Boolean? {
        return settingsProvider.sms_forwarding_switch
    }

    private fun getCodeFromSms(smsMessage: String): String? {
        val pattern = "\\d+".toRegex(RegexOption.IGNORE_CASE)
        return pattern.find(smsMessage)?.value
    }

    fun getSendUrl() = getTelegramUrl() + getApiToken() + getSendOperation() + getChatId()

    fun setPhoneNumber(number: String) {
        settingsProvider.phoneNumber = number
    }

    fun setRegexp(regexp: String) {
        settingsProvider.regexp = regexp
    }

    fun setTelegramUrl(telegramUrl: String) {
        settingsProvider.telegramUrl = telegramUrl
    }

    fun setSendOperation(sendOperation: String) {
        settingsProvider.sendOperation = sendOperation
    }

    fun setApiToken(apiToken: String) {
        settingsProvider.apiToken = apiToken
    }

    fun setChatId(chatId: String) {
        settingsProvider.chatId = chatId
    }

    fun setSmsPermission(permission: String) {
        settingsProvider.sms_permessions = permission
    }

    fun setSmsForwarding(forwarding: Boolean) {
        settingsProvider.sms_forwarding_switch = forwarding
    }

    fun handleSms(messageBody: String?, messageAddress: String?) {
        val phoneNumber = getPhoneNumber()
        val regexp = getRegexp()
        if (messageBody != null) {
            if (messageAddress.equals(phoneNumber) && (regexp?.toRegex()
                    ?.containsMatchIn(messageBody)!!)) {
                getCodeFromSms(messageBody)?.let { saveSmsToDb(it) }
            }
        }
    }

}
