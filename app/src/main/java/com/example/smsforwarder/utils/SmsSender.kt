package com.example.smsforwarder.utils

import android.content.Intent
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_CANCEL_CURRENT
import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.smsforwarder.R
import com.example.smsforwarder.database.SmsInfo
import com.example.smsforwarder.database.Status
import com.example.smsforwarder.service.SmsUpdateService
import kotlinx.coroutines.flow.collect
import okhttp3.*
import java.util.concurrent.TimeUnit


class SmsSender(private val smsHandler: SmsHandler, private val context: Context) {

    private val CHANNEL_ID = "Send error channel"
    private var NOTIFY_ID  = 10
    lateinit var httpClient: OkHttpClient.Builder
    lateinit var okHttpClient: OkHttpClient

    suspend fun startSend() {
        httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(TelegramApiInterceptor())
        httpClient.connectTimeout(10, TimeUnit.SECONDS)
        httpClient.readTimeout(10, TimeUnit.SECONDS)
        okHttpClient = httpClient.build()
        smsHandler.getReadySms().collect { data ->
            data.forEach {
                sendAndUpdateStatus(it)
            }
        }
    }

    private suspend fun sendAndUpdateStatus(sms: SmsInfo) {
        val response = sendSmsToTelegram(sms.smsCode, smsHandler.getSendUrl())
        sms.sendStatus = Status.SEND
        if (response.code != 999){
            sms.sendStatus = Status.SEND
        }
        else {
            sms.sendStatus = Status.FAILED
            sendNotificationWithRetry(sms.smsId, sms.smsCode)
        }
        smsHandler.updateSms(sms)
    }

    private fun sendSmsToTelegram(smsBody: String, sendUrl: String): Response {
        val url = "$sendUrl&text=$smsBody"
        val request = Request.Builder().url(url).build()
        return okHttpClient.newCall(request).execute()
    }

    private fun sendNotificationWithRetry(smsId: Long, smsCode: String) {
        createNotificationChannel(CHANNEL_ID, context, "Send error channel" )
        val notificationIntent = Intent(context, SmsUpdateService::class.java)
        notificationIntent.putExtra("sms_id", smsId)
        val pendingIntent = PendingIntent.getService(context, 1, notificationIntent, FLAG_CANCEL_CURRENT)
        val builder =
            NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.report)
                .setContentTitle("СМС НЕ ОТПРАВЛЕНО")
                .setContentText("Ошибка при пересылке смс")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setGroup("Retry notifications")
                .addAction(R.drawable.reload, "Повторить?", pendingIntent)
                .setAutoCancel(true)
        val notificationManager =
            NotificationManagerCompat.from(context)
        notificationManager.notify(NOTIFY_ID, builder.build())
        NOTIFY_ID++
    }
}

