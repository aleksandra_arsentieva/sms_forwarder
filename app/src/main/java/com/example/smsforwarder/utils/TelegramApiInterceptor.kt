package com.example.smsforwarder.utils

import android.util.Log
import okhttp3.*

class TelegramApiInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        var response: Response = Response.Builder()
            .request(request)
            .protocol(Protocol.HTTP_1_0)
            .code(999)
            .message("Not send")
            .body(ResponseBody.create(null, "{Sms didn't send}"))
            .build()
        var tryCount = 0
        var responseOK = false

        while (!responseOK && tryCount < 3) {
            try {
                Log.d("TAG", "Sending try")
                response = chain.proceed(request)
                responseOK = (response.code == 200)
            } catch (e: Exception) {
                Log.d("TAG", "Request is not successful - $e")
            } finally {
                tryCount++
            }
        }
        return response
    }
}