package com.example.smsforwarder.settings

import android.content.SharedPreferences
import androidx.lifecycle.LiveData


class LiveSharedPreferences(private val sharedPreferences: SharedPreferences) : LiveData<Boolean>() {

    private val mTokenSharedPreferenceListener =
        SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences: SharedPreferences?, key: String? ->
            if (key == MYKEYSTRING) {
                value = sharedPreferences?.getBoolean(MYKEYSTRING, false)
            }
        }


    override fun onActive() {
        super.onActive()
        value = sharedPreferences.getBoolean(MYKEYSTRING, false)
        sharedPreferences.registerOnSharedPreferenceChangeListener(mTokenSharedPreferenceListener)
    }

    override fun onInactive() {
        super.onInactive()
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(mTokenSharedPreferenceListener)
    }

    companion object {
        private const val MYKEYSTRING = "MY_FORWARDING_SMS_VALUE"

    }
}