package com.example.smsforwarder.settings

import android.content.SharedPreferences

interface SettingsProvider {
    var phoneNumber: String?
    var regexp: String?
    var telegramUrl: String?
    var sendOperation: String?
    var apiToken: String?
    var chatId: String?
    var sms_forwarding_switch: Boolean?
    var sms_permessions: String?
}

class SettingsProviderSharedPrefImpl(val sharedPrefernces: SharedPreferences) : SettingsProvider {

    override var phoneNumber: String?
        get() = sharedPrefernces.getString(PHONE_NUMBER_SETTINGS, "")
        set(number) {
            val editor = sharedPrefernces.edit()
            editor.putString(PHONE_NUMBER_SETTINGS, number).apply()
        }

    override var regexp: String?
        get() = sharedPrefernces.getString(REGEXP_SETTINGS, "")
        set(regexp) {
            val editor = sharedPrefernces.edit()
            editor.putString(REGEXP_SETTINGS, regexp).apply()
        }

    override var apiToken: String?
        get() = sharedPrefernces.getString(API_TOKEN_SETTINGS, "")
        set(apiToken) {
            val editor = sharedPrefernces.edit()
            editor.putString(API_TOKEN_SETTINGS, apiToken).apply()
        }

    override var telegramUrl: String?
        get() = sharedPrefernces.getString(TELEGRAM_URL_SETTINGS, "")
        set(telegramUrl) {
            val editor = sharedPrefernces.edit()
            editor.putString(TELEGRAM_URL_SETTINGS, telegramUrl).apply()
        }

    override var sendOperation: String?
        get() = sharedPrefernces.getString(SEND_OPERATION_SETTINGS, "")
        set(sendOperation) {
            val editor = sharedPrefernces.edit()
            editor.putString(SEND_OPERATION_SETTINGS, sendOperation).apply()
        }

    override var chatId: String?
        get() = sharedPrefernces.getString(CHAT_ID_SETTINGS, "")
        set(chatId) {
            val editor = sharedPrefernces.edit()
            editor.putString(CHAT_ID_SETTINGS, chatId).apply()
        }

    override var sms_forwarding_switch: Boolean?
        get() = sharedPrefernces.getBoolean(SETTINGS_SMS_FORWARDING , false)
        set(sms_forwarding_switch) {
            val editor = sharedPrefernces.edit()
            if (sms_forwarding_switch != null) {
                editor.putBoolean(SETTINGS_SMS_FORWARDING, sms_forwarding_switch).apply()
            }
        }

    override var sms_permessions: String?
        get() = sharedPrefernces.getString(SETTINGS_SMS_KEY , "")
        set(sms_permessions) {
            val editor = sharedPrefernces.edit()
            editor.putString(SETTINGS_SMS_KEY, sms_permessions).apply()
        }

    companion object{
        const val PHONE_NUMBER_SETTINGS = "PHONE_NUMBER_SETTINGS"
        const val REGEXP_SETTINGS = "REGEXP_SETTINGS"
        const val API_TOKEN_SETTINGS = "API_TOKEN_SETTINGS"
        const val TELEGRAM_URL_SETTINGS = "TELEGRAM_URL_SETTINGS"
        const val SEND_OPERATION_SETTINGS = "SEND_OPERATION_SETTINGS"
        const val CHAT_ID_SETTINGS = "CHAT_ID_SETTINGS"
        const val SETTINGS_SMS_KEY = "MY_REQUEST_SEND_SMS_VALUE_VER_2"
        const val SETTINGS_SMS_FORWARDING = "MY_FORWARDING_SMS_VALUE"
    }
}