package com.example.smsforwarder

import android.app.Application
import android.content.Context
import com.example.smsforwarder.database.RoomSmsRepository
import com.example.smsforwarder.database.SmsDatabase
import com.example.smsforwarder.settings.SettingsProviderSharedPrefImpl
import com.example.smsforwarder.utils.SmsHandler

class SmsApplication : Application(){
    private lateinit var smsHandler: SmsHandler

    private lateinit var smsRepository: RoomSmsRepository

    override fun onCreate() {
        super.onCreate()
        val database = SmsDatabase.getInstance(this).smsDatabaseDao
        smsRepository = RoomSmsRepository(database)
        smsHandler = SmsHandler(
            SettingsProviderSharedPrefImpl(getSharedPreferences("settings", Context.MODE_PRIVATE)),
            smsRepository
        )
    }

    fun getSmsHandler(): SmsHandler {
        return smsHandler
    }
}